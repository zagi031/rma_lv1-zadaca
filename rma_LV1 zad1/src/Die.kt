class Die {
    var value: Int = 0
        private set

    fun roll() {
        this.value = (1..6).random()
    }

    override fun toString(): String {
        return "Value: $value"
    }
}