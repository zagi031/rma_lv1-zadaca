class YahtzeeChecker : DiceChecker {
    override fun check(dice: List<Die>): String {
        if (dice.groupingBy { it.value }.eachCount().filter { it.value >= 5 }.isNotEmpty()) {
            return "Yahtzee!"
        } else return ""
    }
}