fun main() {
    val dice = mutableListOf<Die>()
    dice.add(Die())
    dice.add(Die())
    dice.add(Die())
    dice.add(Die())
    dice.add(Die())
    dice.add(Die())
    val checkers = mutableListOf<DiceChecker>()
    checkers.add(YahtzeeChecker())
    checkers.add(FourOfAKindChecker())
    checkers.add(ThreeOfAKindChecker())
    checkers.add(LargeStraightChecker())
    checkers.add(SmallStraightChecker())

    val diceIndexesToRoll = mutableListOf<Int>()
    while (true) {
        diceIndexesToRoll.clear()
        rollDice(dice, listOf(0, 1, 2, 3, 4, 5))    //initial rolling
        printDiceValues(dice)
        dice.forEachIndexed { index, die ->
            println("Roll Dice[$index]: ${die.value}? Type 'y' for yes, 'n' for no.")
            val userInput = readLine()
            if (userInput == "y") {
                diceIndexesToRoll.add(index)
            }
        }
        rollDice(dice, diceIndexesToRoll)   //user roll
        printDiceValues(dice)
        println(checkDice(dice, checkers))
        Thread.sleep(2000)
    }
}

fun rollDice(dice: List<Die>, diceIndexesToRoll: List<Int>) {
    dice.forEachIndexed { index, die ->
        if (diceIndexesToRoll.contains(index)) {
            println("Rolling at index=$index...")
            die.roll()
        }
    }
}

fun printDiceValues(dice: List<Die>) {
    dice.forEachIndexed { index, it ->
        print("Dice[$index]: ${it.value}\t")
    }
    println()
}

fun checkDice(dice: List<Die>, checkers: List<DiceChecker>): String {
    var result = ""
    checkers.forEach {
        result += "${it.check(dice)}\n"
    }
    return result.trim()
}