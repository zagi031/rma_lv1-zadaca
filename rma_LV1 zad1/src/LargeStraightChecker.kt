class LargeStraightChecker : DiceChecker {
    override fun check(dice: List<Die>): String {
        val orderedDiceValues = dice.map { it.value }.sortedBy { it }.toSet().toList().asReversed()
        if (orderedDiceValues.size < 5) {
            return ""
        }
        if (orderedDiceValues.first() != 6) {
            return ""
        }
        for (i in 0..3) {
            if (orderedDiceValues[i] != orderedDiceValues[i + 1] + 1) {
                return ""
            }
        }
        return "Large Straight!"
    }
}