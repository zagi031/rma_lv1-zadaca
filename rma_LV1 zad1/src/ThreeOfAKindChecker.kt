class ThreeOfAKindChecker : DiceChecker {
    override fun check(dice: List<Die>): String {
        return if (dice.groupingBy { it.value }.eachCount().filter { it.value >= 3 }.isNotEmpty()) {
            "Three-Of-A-Kind!"
        } else ""
    }
}