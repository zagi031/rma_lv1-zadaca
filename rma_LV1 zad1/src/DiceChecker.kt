interface DiceChecker {
    fun check(dice: List<Die>): String
}