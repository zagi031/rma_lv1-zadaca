class FourOfAKindChecker : DiceChecker {
    override fun check(dice: List<Die>): String {
        return if (dice.groupingBy { it.value }.eachCount().filter { it.value >= 4 }.isNotEmpty()) {
            "Four-Of-A-Kind!"
        } else ""
    }
}