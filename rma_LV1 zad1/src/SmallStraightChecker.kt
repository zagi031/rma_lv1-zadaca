class SmallStraightChecker : DiceChecker {
    override fun check(dice: List<Die>): String {
        val orderedDiceValues = dice.map { it.value }.sortedBy { it }.toSet().toList()
        if (orderedDiceValues.size < 5) {
            return ""
        }
        if (orderedDiceValues.first() != 1) {
            return ""
        }
        for (i in 0..3) {
            if (orderedDiceValues[i] + 1 != orderedDiceValues[i + 1]) {
                return ""
            }
        }
        return "Small Straight!"
    }
}