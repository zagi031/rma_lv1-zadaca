class Player(private val isDealer: Boolean) {
    val hand = mutableListOf<Card>()

    fun drawCard(deck: MutableList<Card>) {
        val card = deck[(0 until deck.size).random()]
        hand.add(card)
        deck.remove(card)
    }

    fun checkIfBusted() = getSumFromHands() > 21

    fun getSumFromHands(): Int {
        var result = 0
        hand.forEach { result += it.value }

        //if ace counts as 1 or 11 only for dealer
        //then this 'if expression' should be nested inside if(isDealer){}
        if (hand.map { it.name }.contains("ace") && result > 21) {
            return result - 10  //ace counts as 1
        }

        return result
    }

    fun hasBlackJack() = getSumFromHands() == 21

    fun printHand() {
        var text = ""
        text += if (isDealer) {
            "Dealers hand:\n"
        } else {
            "Users hand:\n"
        }
        hand.forEach {
            text += "${it.name} of ${it.type}\t"
        }
        text += "Sum: ${getSumFromHands()}"
        println(text)
    }
}