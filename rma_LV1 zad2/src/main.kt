fun main() {
    val pokerDeck = initializePokerDeck()
    var userInput: String
    val player = Player(false)
    val dealer = Player(true)

    while (true) {
        userInput = ""
        pokerDeck.shuffle()
        player.drawCard(pokerDeck)  //user draws two cards
        player.drawCard(pokerDeck)
        player.printHand()

        while (!player.checkIfBusted() && userInput != "stand" && !player.hasBlackJack()) { //users turn
            println("For hit type 'hit', for stand type 'stand'")
            userInput = readLine()?.trim().toString()
            if (userInput == "hit") {
                player.drawCard(pokerDeck)
                if (player.checkIfBusted()) {
                    print("User busted. ")
                }
                player.printHand()
            } else if (userInput == "stand") {
                break
            } else {
                println("Incorrect command.")
            }
        }

        dealer.drawCard(pokerDeck)  //dealer draws two cards
        dealer.drawCard(pokerDeck)
        dealer.printHand()
        while (!player.checkIfBusted() && !dealer.checkIfBusted() && dealer.getSumFromHands() < 17) {  //dealers turn, dealer draws cards if his hand sum is lower than 17
            println("Dealer draw a card.")
            dealer.drawCard(pokerDeck)
            if (dealer.checkIfBusted()) {
                print("Dealer busted. ")
            }
            dealer.printHand()
        }

        println("${checkWinner(player, dealer)}\n")

        returnCards(player.hand, pokerDeck) //returning cards to deck
        returnCards(dealer.hand, pokerDeck)
        Thread.sleep(1000)
    }
}

fun initializePokerDeck(): MutableList<Card> {
    val deck = mutableListOf<Card>()
    deck.add(Card("two", 2, CardType.clubs))
    deck.add(Card("three", 3, CardType.clubs))
    deck.add(Card("four", 4, CardType.clubs))
    deck.add(Card("five", 5, CardType.clubs))
    deck.add(Card("six", 6, CardType.clubs))
    deck.add(Card("seven", 7, CardType.clubs))
    deck.add(Card("eight", 8, CardType.clubs))
    deck.add(Card("nine", 9, CardType.clubs))
    deck.add(Card("ten", 10, CardType.clubs))
    deck.add(Card("jack", 10, CardType.clubs))
    deck.add(Card("queen", 10, CardType.clubs))
    deck.add(Card("king", 10, CardType.clubs))
    deck.add(Card("ace", 11, CardType.clubs))

    deck.add(Card("two", 2, CardType.diamonds))
    deck.add(Card("three", 3, CardType.diamonds))
    deck.add(Card("four", 4, CardType.diamonds))
    deck.add(Card("five", 5, CardType.diamonds))
    deck.add(Card("six", 6, CardType.diamonds))
    deck.add(Card("seven", 7, CardType.diamonds))
    deck.add(Card("eight", 8, CardType.diamonds))
    deck.add(Card("nine", 9, CardType.diamonds))
    deck.add(Card("ten", 10, CardType.diamonds))
    deck.add(Card("jack", 10, CardType.diamonds))
    deck.add(Card("queen", 10, CardType.diamonds))
    deck.add(Card("king", 10, CardType.diamonds))
    deck.add(Card("ace", 11, CardType.diamonds))

    deck.add(Card("two", 2, CardType.hearts))
    deck.add(Card("three", 3, CardType.hearts))
    deck.add(Card("four", 4, CardType.hearts))
    deck.add(Card("five", 5, CardType.hearts))
    deck.add(Card("six", 6, CardType.hearts))
    deck.add(Card("seven", 7, CardType.hearts))
    deck.add(Card("eight", 8, CardType.hearts))
    deck.add(Card("nine", 9, CardType.hearts))
    deck.add(Card("ten", 10, CardType.hearts))
    deck.add(Card("jack", 10, CardType.hearts))
    deck.add(Card("queen", 10, CardType.hearts))
    deck.add(Card("king", 10, CardType.hearts))
    deck.add(Card("ace", 11, CardType.hearts))

    deck.add(Card("two", 2, CardType.spades))
    deck.add(Card("three", 3, CardType.spades))
    deck.add(Card("four", 4, CardType.spades))
    deck.add(Card("five", 5, CardType.spades))
    deck.add(Card("six", 6, CardType.spades))
    deck.add(Card("seven", 7, CardType.spades))
    deck.add(Card("eight", 8, CardType.spades))
    deck.add(Card("nine", 9, CardType.spades))
    deck.add(Card("ten", 10, CardType.spades))
    deck.add(Card("jack", 10, CardType.spades))
    deck.add(Card("queen", 10, CardType.spades))
    deck.add(Card("king", 10, CardType.spades))
    deck.add(Card("ace", 11, CardType.spades))

    return deck
}

fun returnCards(playersHand: MutableList<Card>, originalDeck: MutableList<Card>) {
    playersHand.forEach {
        originalDeck.add(it)
    }
    playersHand.clear()
}

fun checkWinner(player: Player, dealer: Player): String {
    return if ((player.getSumFromHands() > dealer.getSumFromHands() && !player.checkIfBusted()) || dealer.checkIfBusted()) {
        "**********|User wins!|**********"
    } else if (player.getSumFromHands() == dealer.getSumFromHands()) {
        "**********|Draw!|**********"
    } else "**********|Dealer wins!|**********"
}