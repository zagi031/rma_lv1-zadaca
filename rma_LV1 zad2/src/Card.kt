enum class CardType {
    hearts, clubs, diamonds, spades
}

data class Card(val name: String, val value: Int, val type: CardType) {
    override fun toString(): String {
        return "$name of $type"
    }
}